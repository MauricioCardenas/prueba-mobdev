package com.pruebamobdev.services;


import com.pruebamobdev.entities.response.ObtenerLocacionResponse;
import com.pruebamobdev.entities.response.ObtenerPersonajeResponse;
import com.pruebamobdev.entities.response.OriginResponseDTO;
import com.pruebamobdev.entities.response.RootResponse;
import com.pruebamobdev.repository.LocationRepository;
import com.pruebamobdev.repository.PersonajeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RickAndMortyServices {

    private static Logger LOG = LoggerFactory.getLogger(RickAndMortyServices.class);

    @Autowired
    private PersonajeRepository personajeRepository;

    @Autowired
    private LocationRepository locationRepository;

    public RootResponse obtenerPersonaje(int id) {
        RootResponse rootResponse = new RootResponse();
        ObtenerPersonajeResponse obtenerPersonajeResponse = personajeRepository.obtenerPersonaje(id);
        ObtenerLocacionResponse obtenerLocacionResponse = locationRepository.obtenerLocacion(obtenerPersonajeResponse.getOrigin().getUrl());
        rootResponse = convertirRespuesta(obtenerPersonajeResponse, obtenerLocacionResponse);

        return rootResponse;
    }

    private RootResponse convertirRespuesta(ObtenerPersonajeResponse obtenerPersonajeResponse, ObtenerLocacionResponse obtenerLocacionResponse) {
        RootResponse rootResponse = new RootResponse();

        rootResponse.setId(obtenerPersonajeResponse.getId());
        rootResponse.setName(obtenerPersonajeResponse.getName());
        rootResponse.setStatus(obtenerPersonajeResponse.getStatus());
        rootResponse.setSpecies(obtenerPersonajeResponse.getSpecies());
        rootResponse.setType(obtenerPersonajeResponse.getType());
        rootResponse.setEpisode_count(obtenerPersonajeResponse.getEpisode().size());

        OriginResponseDTO originResponseDTO = new OriginResponseDTO();
        originResponseDTO.setName(obtenerPersonajeResponse.getOrigin().getName());
        originResponseDTO.setUrl(obtenerPersonajeResponse.getOrigin().getUrl());
        originResponseDTO.setDimension(obtenerLocacionResponse.getDimension());
        originResponseDTO.setResidents(obtenerLocacionResponse.getResidents());
        rootResponse.setOrigin(originResponseDTO);
        return rootResponse;
    }


}
