package com.pruebamobdev.controller;

import com.pruebamobdev.services.RickAndMortyServices;
import com.pruebamobdev.entities.response.RootResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value="RickAndMorty/api")
public class PersonajeController {

    @Autowired
    private RickAndMortyServices rickAndMortyServices;

    @GetMapping(value = "obtenerPersonaje/{id}")
    public ResponseEntity<RootResponse> obtenerPersonaje(@PathVariable(value = "id") int id) {

        RootResponse response = rickAndMortyServices.obtenerPersonaje(id);
        return ResponseEntity.ok(response);
    }


}
