package com.pruebamobdev.repository;

import com.pruebamobdev.entities.response.ObtenerPersonajeResponse;

public interface PersonajeRepository {

    public ObtenerPersonajeResponse obtenerPersonaje(int id);
}
