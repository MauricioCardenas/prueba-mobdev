package com.pruebamobdev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaMobdevApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaMobdevApplication.class, args);
    }



}
