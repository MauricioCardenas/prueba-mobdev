package com.pruebamobdev.repository.impl;

import com.pruebamobdev.core.exception.CharacterNotFoundException;
import com.pruebamobdev.entities.response.ObtenerPersonajeResponse;
import com.pruebamobdev.repository.PersonajeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class PersonajeRepositoryImpl implements PersonajeRepository {

    private static Logger LOG = LoggerFactory.getLogger(PersonajeRepositoryImpl.class);

    @Value("${url.getPersonaje}")
    private String urlGetPersonaje;

    @Override
    public ObtenerPersonajeResponse obtenerPersonaje(int id) {
        LOG.info("Entro a obtenerPersonaje");

        final String url = urlGetPersonaje + id;

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = getHeaders();
        HttpEntity<ObtenerPersonajeResponse> httpEntity = new HttpEntity<>(httpHeaders);
        ObtenerPersonajeResponse obtenerPersonajeResponse = new ObtenerPersonajeResponse();
        try {
            ResponseEntity<ObtenerPersonajeResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, ObtenerPersonajeResponse.class);

            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                obtenerPersonajeResponse = responseEntity.getBody();
                LOG.info("Se ha obtenido al personaje : " + obtenerPersonajeResponse.getName());
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            if (e.getMessage().contains("Character not found")) {
                throw new CharacterNotFoundException("Personaje no encontrado");
            }
        }

        return obtenerPersonajeResponse;
    }

    private static HttpHeaders getHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Accept", "application/json");
        httpHeaders.set("Content-Type", "application/json");

        return httpHeaders;
    }

}
