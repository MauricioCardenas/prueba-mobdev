package com.pruebamobdev.entities.response;

import java.util.List;

public class OriginResponseDTO {

    private String name;
    private String url;
    private String dimension;
    private List residents;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public List getResidents() {
        return residents;
    }

    public void setResidents(List residents) {
        this.residents = residents;
    }
}
