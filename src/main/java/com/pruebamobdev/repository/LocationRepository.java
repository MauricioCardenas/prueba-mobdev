package com.pruebamobdev.repository;

import com.pruebamobdev.entities.response.ObtenerLocacionResponse;

public interface LocationRepository {

    public ObtenerLocacionResponse obtenerLocacion(String url);
}
