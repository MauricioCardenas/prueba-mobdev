package com.pruebamobdev.repository.impl;

import com.pruebamobdev.entities.response.ObtenerLocacionResponse;
import com.pruebamobdev.repository.LocationRepository;
import jdk.internal.org.jline.utils.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
@Repository
public class LocationRepositoryImpl implements LocationRepository {

    private static Logger LOG = LoggerFactory.getLogger(LocationRepositoryImpl.class);



    @Override
    public ObtenerLocacionResponse obtenerLocacion(String url) {
        LOG.info("Entro a obtenerLocacion con la url -->"+url);
        RestTemplate restTemplate = new RestTemplate();
        ObtenerLocacionResponse obtenerLocacionResponse = restTemplate.getForObject(url, ObtenerLocacionResponse.class);
        LOG.info("Se ha obtenido la dimension : "+obtenerLocacionResponse.getDimension());
        return obtenerLocacionResponse;
    }
}
