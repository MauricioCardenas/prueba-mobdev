package com.pruebamobdev;

import com.pruebamobdev.entities.response.ObtenerLocacionResponse;
import com.pruebamobdev.entities.response.RootResponse;
import com.pruebamobdev.repository.LocationRepository;
import com.pruebamobdev.services.RickAndMortyServices;
import org.junit.jupiter.api.*;
import org.junit.platform.commons.annotation.Testable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.Assert;


@SpringBootTest
class PruebaMobdevApplicationTests {

    @Autowired
    private RickAndMortyServices rickAndMortyServices;

    @Autowired
    private LocationRepository locationRepository;

    @Testable
    void contextLoads() {

    }

    @Test
    void obtenerPersonaje() {

        RootResponse rootResponse = rickAndMortyServices.obtenerPersonaje(1);
        System.out.println(rootResponse.getName());
        Assert.assertEquals("Rick Sanchez",rootResponse.getName());
    }


    @Test
    void obtenerLocacion() {

        ObtenerLocacionResponse obtenerLocacionResponse = locationRepository.obtenerLocacion("https://rickandmortyapi.com/api/location/1");
        System.out.println(obtenerLocacionResponse.getDimension());
        Assert.assertEquals("Dimension C-137",obtenerLocacionResponse.getDimension());
    }


}
